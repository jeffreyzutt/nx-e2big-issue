import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller72 from './some-long-name-for-this-library-number-filler-filler-filler72';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller72', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller72 />
    );
    expect(baseElement).toBeTruthy();
  });
});
