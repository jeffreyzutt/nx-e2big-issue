import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller08 from './some-long-name-for-this-library-number-filler-filler-filler08';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller08', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller08 />
    );
    expect(baseElement).toBeTruthy();
  });
});
