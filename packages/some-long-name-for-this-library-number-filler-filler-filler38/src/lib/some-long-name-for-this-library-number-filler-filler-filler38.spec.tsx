import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller38 from './some-long-name-for-this-library-number-filler-filler-filler38';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller38', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller38 />
    );
    expect(baseElement).toBeTruthy();
  });
});
