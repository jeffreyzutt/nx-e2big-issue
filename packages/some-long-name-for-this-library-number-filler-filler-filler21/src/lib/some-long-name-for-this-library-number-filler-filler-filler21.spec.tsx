import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller21 from './some-long-name-for-this-library-number-filler-filler-filler21';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller21', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller21 />
    );
    expect(baseElement).toBeTruthy();
  });
});
