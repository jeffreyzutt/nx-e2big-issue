import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller25 from './some-long-name-for-this-library-number-filler-filler-filler25';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller25', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller25 />
    );
    expect(baseElement).toBeTruthy();
  });
});
