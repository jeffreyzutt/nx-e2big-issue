import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller02 from './some-long-name-for-this-library-number-filler-filler-filler02';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller02', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller02 />
    );
    expect(baseElement).toBeTruthy();
  });
});
