import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller56 from './some-long-name-for-this-library-number-filler-filler-filler56';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller56', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller56 />
    );
    expect(baseElement).toBeTruthy();
  });
});
