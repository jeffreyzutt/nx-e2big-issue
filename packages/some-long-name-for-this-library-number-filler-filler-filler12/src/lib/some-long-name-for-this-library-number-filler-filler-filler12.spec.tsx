import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller12 from './some-long-name-for-this-library-number-filler-filler-filler12';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller12', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller12 />
    );
    expect(baseElement).toBeTruthy();
  });
});
