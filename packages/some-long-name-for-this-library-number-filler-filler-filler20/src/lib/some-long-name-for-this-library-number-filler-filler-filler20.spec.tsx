import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller20 from './some-long-name-for-this-library-number-filler-filler-filler20';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller20', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller20 />
    );
    expect(baseElement).toBeTruthy();
  });
});
