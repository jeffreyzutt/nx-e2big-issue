import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller65 from './some-long-name-for-this-library-number-filler-filler-filler65';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller65', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller65 />
    );
    expect(baseElement).toBeTruthy();
  });
});
