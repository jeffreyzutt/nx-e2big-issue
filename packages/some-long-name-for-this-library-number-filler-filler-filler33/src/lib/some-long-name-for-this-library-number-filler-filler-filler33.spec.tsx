import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller33 from './some-long-name-for-this-library-number-filler-filler-filler33';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller33', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller33 />
    );
    expect(baseElement).toBeTruthy();
  });
});
