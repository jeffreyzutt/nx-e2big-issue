import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller15 from './some-long-name-for-this-library-number-filler-filler-filler15';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller15', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller15 />
    );
    expect(baseElement).toBeTruthy();
  });
});
