import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller13 from './some-long-name-for-this-library-number-filler-filler-filler13';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller13', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller13 />
    );
    expect(baseElement).toBeTruthy();
  });
});
