import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller10 from './some-long-name-for-this-library-number-filler-filler-filler10';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller10', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller10 />
    );
    expect(baseElement).toBeTruthy();
  });
});
