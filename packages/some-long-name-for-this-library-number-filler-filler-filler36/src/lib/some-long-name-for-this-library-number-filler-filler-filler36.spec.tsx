import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller36 from './some-long-name-for-this-library-number-filler-filler-filler36';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller36', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller36 />
    );
    expect(baseElement).toBeTruthy();
  });
});
