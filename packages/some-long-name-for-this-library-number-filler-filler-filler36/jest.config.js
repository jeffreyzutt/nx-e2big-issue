module.exports = {
  displayName: 'some-long-name-for-this-library-number-filler-filler-filler36',
  preset: '../../jest.preset.js',
  transform: {
    '^.+\\.[tj]sx?$': 'babel-jest',
  },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
  coverageDirectory:
    '../../coverage/packages/some-long-name-for-this-library-number-filler-filler-filler36',
};
