import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller03 from './some-long-name-for-this-library-number-filler-filler-filler03';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller03', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller03 />
    );
    expect(baseElement).toBeTruthy();
  });
});
