import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller17 from './some-long-name-for-this-library-number-filler-filler-filler17';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller17', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller17 />
    );
    expect(baseElement).toBeTruthy();
  });
});
