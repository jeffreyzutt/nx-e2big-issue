import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller11 from './some-long-name-for-this-library-number-filler-filler-filler11';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller11', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller11 />
    );
    expect(baseElement).toBeTruthy();
  });
});
