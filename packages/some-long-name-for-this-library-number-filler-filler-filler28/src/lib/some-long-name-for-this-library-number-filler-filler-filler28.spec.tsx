import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller28 from './some-long-name-for-this-library-number-filler-filler-filler28';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller28', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller28 />
    );
    expect(baseElement).toBeTruthy();
  });
});
