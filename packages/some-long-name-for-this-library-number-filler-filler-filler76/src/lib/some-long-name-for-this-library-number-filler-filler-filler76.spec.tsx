import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller76 from './some-long-name-for-this-library-number-filler-filler-filler76';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller76', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller76 />
    );
    expect(baseElement).toBeTruthy();
  });
});
