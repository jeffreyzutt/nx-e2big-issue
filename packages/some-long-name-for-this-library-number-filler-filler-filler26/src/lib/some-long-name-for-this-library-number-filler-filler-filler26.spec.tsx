import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller26 from './some-long-name-for-this-library-number-filler-filler-filler26';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller26', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller26 />
    );
    expect(baseElement).toBeTruthy();
  });
});
