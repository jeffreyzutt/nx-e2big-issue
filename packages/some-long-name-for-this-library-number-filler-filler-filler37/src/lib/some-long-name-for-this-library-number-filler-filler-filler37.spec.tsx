import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller37 from './some-long-name-for-this-library-number-filler-filler-filler37';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller37', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller37 />
    );
    expect(baseElement).toBeTruthy();
  });
});
