import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller27 from './some-long-name-for-this-library-number-filler-filler-filler27';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller27', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller27 />
    );
    expect(baseElement).toBeTruthy();
  });
});
