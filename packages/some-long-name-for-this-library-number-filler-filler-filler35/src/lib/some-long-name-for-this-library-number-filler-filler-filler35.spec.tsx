import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller35 from './some-long-name-for-this-library-number-filler-filler-filler35';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller35', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller35 />
    );
    expect(baseElement).toBeTruthy();
  });
});
