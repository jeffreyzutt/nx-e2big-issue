import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller77 from './some-long-name-for-this-library-number-filler-filler-filler77';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller77', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller77 />
    );
    expect(baseElement).toBeTruthy();
  });
});
