import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller41 from './some-long-name-for-this-library-number-filler-filler-filler41';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller41', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller41 />
    );
    expect(baseElement).toBeTruthy();
  });
});
