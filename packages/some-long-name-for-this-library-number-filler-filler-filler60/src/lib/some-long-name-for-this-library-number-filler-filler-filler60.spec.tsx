import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller60 from './some-long-name-for-this-library-number-filler-filler-filler60';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller60', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller60 />
    );
    expect(baseElement).toBeTruthy();
  });
});
