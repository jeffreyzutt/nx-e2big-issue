import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller48 from './some-long-name-for-this-library-number-filler-filler-filler48';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller48', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller48 />
    );
    expect(baseElement).toBeTruthy();
  });
});
