import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller32 from './some-long-name-for-this-library-number-filler-filler-filler32';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller32', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller32 />
    );
    expect(baseElement).toBeTruthy();
  });
});
