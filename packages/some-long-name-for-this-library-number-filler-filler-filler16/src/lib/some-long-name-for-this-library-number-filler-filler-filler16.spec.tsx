import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller16 from './some-long-name-for-this-library-number-filler-filler-filler16';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller16', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller16 />
    );
    expect(baseElement).toBeTruthy();
  });
});
