import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller05 from './some-long-name-for-this-library-number-filler-filler-filler05';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller05', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller05 />
    );
    expect(baseElement).toBeTruthy();
  });
});
