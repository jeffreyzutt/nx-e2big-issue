import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller22 from './some-long-name-for-this-library-number-filler-filler-filler22';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller22', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller22 />
    );
    expect(baseElement).toBeTruthy();
  });
});
