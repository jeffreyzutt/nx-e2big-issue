import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller70 from './some-long-name-for-this-library-number-filler-filler-filler70';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller70', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller70 />
    );
    expect(baseElement).toBeTruthy();
  });
});
