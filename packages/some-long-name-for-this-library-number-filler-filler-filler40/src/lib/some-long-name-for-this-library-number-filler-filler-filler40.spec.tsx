import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller40 from './some-long-name-for-this-library-number-filler-filler-filler40';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller40', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller40 />
    );
    expect(baseElement).toBeTruthy();
  });
});
