import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller30 from './some-long-name-for-this-library-number-filler-filler-filler30';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller30', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller30 />
    );
    expect(baseElement).toBeTruthy();
  });
});
