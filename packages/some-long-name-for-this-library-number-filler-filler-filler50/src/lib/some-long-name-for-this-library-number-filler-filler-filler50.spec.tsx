import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller50 from './some-long-name-for-this-library-number-filler-filler-filler50';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller50', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller50 />
    );
    expect(baseElement).toBeTruthy();
  });
});
