import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller63 from './some-long-name-for-this-library-number-filler-filler-filler63';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller63', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller63 />
    );
    expect(baseElement).toBeTruthy();
  });
});
