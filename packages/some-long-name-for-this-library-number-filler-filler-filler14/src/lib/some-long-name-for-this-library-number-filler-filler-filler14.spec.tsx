import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller14 from './some-long-name-for-this-library-number-filler-filler-filler14';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller14', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller14 />
    );
    expect(baseElement).toBeTruthy();
  });
});
