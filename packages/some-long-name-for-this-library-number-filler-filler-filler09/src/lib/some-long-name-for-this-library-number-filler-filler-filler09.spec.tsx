import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller09 from './some-long-name-for-this-library-number-filler-filler-filler09';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller09', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller09 />
    );
    expect(baseElement).toBeTruthy();
  });
});
