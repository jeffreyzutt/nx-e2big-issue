import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller23 from './some-long-name-for-this-library-number-filler-filler-filler23';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller23', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller23 />
    );
    expect(baseElement).toBeTruthy();
  });
});
