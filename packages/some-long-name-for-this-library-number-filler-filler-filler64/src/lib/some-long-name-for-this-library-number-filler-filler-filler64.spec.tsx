import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller64 from './some-long-name-for-this-library-number-filler-filler-filler64';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller64', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller64 />
    );
    expect(baseElement).toBeTruthy();
  });
});
