import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller06 from './some-long-name-for-this-library-number-filler-filler-filler06';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller06', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller06 />
    );
    expect(baseElement).toBeTruthy();
  });
});
