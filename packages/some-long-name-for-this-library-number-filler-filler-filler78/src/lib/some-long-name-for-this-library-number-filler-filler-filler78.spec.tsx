import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller78 from './some-long-name-for-this-library-number-filler-filler-filler78';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller78', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller78 />
    );
    expect(baseElement).toBeTruthy();
  });
});
