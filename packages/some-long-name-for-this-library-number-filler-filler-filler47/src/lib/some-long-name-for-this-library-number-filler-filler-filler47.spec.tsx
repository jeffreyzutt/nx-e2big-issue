import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller47 from './some-long-name-for-this-library-number-filler-filler-filler47';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller47', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller47 />
    );
    expect(baseElement).toBeTruthy();
  });
});
