import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller42 from './some-long-name-for-this-library-number-filler-filler-filler42';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller42', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller42 />
    );
    expect(baseElement).toBeTruthy();
  });
});
