import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller46 from './some-long-name-for-this-library-number-filler-filler-filler46';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller46', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller46 />
    );
    expect(baseElement).toBeTruthy();
  });
});
