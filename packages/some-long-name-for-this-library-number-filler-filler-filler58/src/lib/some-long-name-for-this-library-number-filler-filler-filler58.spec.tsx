import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller58 from './some-long-name-for-this-library-number-filler-filler-filler58';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller58', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller58 />
    );
    expect(baseElement).toBeTruthy();
  });
});
