import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller18 from './some-long-name-for-this-library-number-filler-filler-filler18';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller18', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller18 />
    );
    expect(baseElement).toBeTruthy();
  });
});
