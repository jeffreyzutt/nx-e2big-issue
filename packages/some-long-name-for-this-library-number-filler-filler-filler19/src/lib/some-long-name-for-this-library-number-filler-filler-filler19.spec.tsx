import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller19 from './some-long-name-for-this-library-number-filler-filler-filler19';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller19', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller19 />
    );
    expect(baseElement).toBeTruthy();
  });
});
