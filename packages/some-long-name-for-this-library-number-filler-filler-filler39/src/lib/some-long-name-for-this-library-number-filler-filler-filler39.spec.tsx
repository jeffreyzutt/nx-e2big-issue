import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller39 from './some-long-name-for-this-library-number-filler-filler-filler39';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller39', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller39 />
    );
    expect(baseElement).toBeTruthy();
  });
});
