import { render } from '@testing-library/react';

import SomeLongNameForThisLibraryNumberFillerFillerFiller73 from './some-long-name-for-this-library-number-filler-filler-filler73';

describe('SomeLongNameForThisLibraryNumberFillerFillerFiller73', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <SomeLongNameForThisLibraryNumberFillerFillerFiller73 />
    );
    expect(baseElement).toBeTruthy();
  });
});
